function sum(a) {
  
  var sum = a 
  
  function f(b) {
    sum += b
    return f
  }
  
  f.toString = function() { return sum }
  
  return f
}
//alert( sum(0)(1)(2)(3)(4)(5) )  // 15
