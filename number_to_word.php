<?php 
/** CONVERT NUMBER TO WORDS */
function number_to_word($num) {

	$constUnit = array(
		0 => 'zero',
		1 => 'one',
		2 => 'two',
		3 => 'three',
		4 => 'four',
		5 => 'five',
		6 => 'six',
		7 => 'seven',
		8 => 'eight',
		9 => 'nine'
	);

	$constTeen = array(
		10 => 'ten',
		11 => 'eleven',
		12 => 'twelve',
		13 => 'thirteen',
		14 => 'fourteen',
		15 => 'fifteen',
		16 => 'sixteen',
		17 => 'seventeen',
		18 => 'eighteen',
		19 => 'nineteen'
	);
	
	$constTens = array(
		2 => 'twenty',
		3 => 'thirty',
		4 => 'fourty',
		5 => 'fifty',
		6 => 'sixty',
		7 => 'seventy',
		8 => 'eighty',
		9 => 'ninety'
	);
	
	
	$size = strlen($num);
	$temp = $num;
	for ($i = $size; $i > 0; $i--) {
		$div = pow(10,($i-1));
		switch($div) {
			case 1000:
				$x = floor($temp/1000);
				$temp = $temp%1000;
				if ($x == 1 && $temp == 0) { 
					echo "{$constUnit[$x]} thousand";
					break 2;
				}
				echo "{$constUnit[$x]} thousand ";
				break;
			case 100:
				$x = floor($temp/100);
				$temp = $temp%100;
				if ($x == 0) {
					break 1;
				}
				if ($x == 1 && $temp == 0) {
					echo "{$constUnit[$x]} hundred";
					break 2;
				}
				echo "{$constUnit[$x]} hundred and ";
				break;
			case 10:
				$x = floor($temp/10);
				$temp2 = $temp;
				$temp = $temp%10;
				if ($x == 0) {
					echo " ";
				} else if ($x == 1) {
					echo "{$constTeen[$temp2]} ";
					break 2;
				} else {
					echo "{$constTens[$x]} ";
					if($temp%10 == 0) {
						break 2;
					}
				}
				break;
			case 1:
				echo $constUnit[$temp];
				break;
		}
	}
	
}
//number_to_word(100); // One hundred