<?php

/**
 *  Basic algorithms
 */

/** REVERSE STRING */ 
function reverse_string($str) { 
	return ($str) ? substr($str, -1) . reverse_string(substr($str, 0, -1)) : "";
}
//echo reverse_string("My name is Satish"); // Output : hsitaS si eman yM

/** REVERSE ONLY WORDS IN STRING */
/**
 *  This can be done by reversing order of words i.e Satish is name My
 *  then reversing as above which is yM eman si hsitaS
 */

/** REVERSE INTEGERS */
function reverse_integer($num) {
    $rev = 0;
    while ($num > 0) {
        $rev = ($rev *10)+($num % 10); 
        $num = (int)($num / 10 ); 
    }
    return $rev;
}
//var_dump(reverse_integer(123456)); // 654321

/** CHECK PALINDROME */
function check_palindrome($str) {
	$len = strlen($str);
	for ($i = 0; $i < $len/2; $i++) {
		if ($str[$i] != $str[$len-$i-1]) {
			return false;
		}
	}
	return true;
}
//var_dump(check_palindrome("12399321")); // True

/** FIBONACCI */
function fibonacci($num) {
	if ($num <= 2) {
		return 1;
	}
	return fibonacci($num-1) + fibonacci($num-2);
}
//echo fibonacci(6); 

/** EXPONENTIAL */
function exponential($num) {
	if ($num <= 1) {
		return 1;
	}
	return $num * exponential($num-1);
}
//echo exponential(5); // 5*4*3*2*1 = 120

/**
 * Flatten Multi dimentional array
 * multi- dimentional array => single dimentional
 */
function flatten($input) {
    $result = array();
    foreach ($input as $k => $v) {
        if (is_array($v)) {
            $result = array_merge($result, flatten($v));
        } else {
            $result[] = $v;
        }
    }
    return $result;
}
$input = array(1, 2, array(3, 4), 5, array(6, array(7, 8), 9));
//$r = flatten($input); var_dump($r); // output => array(1,2,3,4,5,6,7,8,9)

/** SUM PAIRS */
function find_sum_pairs($aInput, $sum) {
	$aPairCheck = array();
	for ($i = 0; $i < sizeof($aInput); $i++) {
		if (array_key_exists($aInput[$i], $aPairCheck)) {
			echo $aInput[$i] . ", " .  $aPairCheck[$aInput[$i]] . "<br>";
		} else {
			$aPairCheck[$sum - $aInput[$i]] = $aInput[$i];
		}
	}

}
find_sum_pairs(array(1,2,3,4,5,6,9), 11); // Output: 6,5 9,2 <== Unique pairs


/** COMBINATION OF THREE NUMBERS TO SUM */
function find_combination_sum($aInput, $sum) {
	// Added two number and find third number = $sum - (sum of first two number)
	//sort($aInput);
	$aOutput = array();
	$iSize = sizeof($aInput);
	$iLoop = $iSize*$iSize;
	for ($i = 0, $j = 0, $k = -1; $i < $iLoop; $i++, $j++) {
		if ($i%$iSize == 0) {
			$j = 0;
			$k++;
			$firstNumber = $aInput[$k]; 
		}
		if ($j != $k) {
			$secondNumber = $aInput[$j];
			$sumOfTwo = $firstNumber + $secondNumber;
			$possibleThirdNumber = $sum - $sumOfTwo;
			if (in_array($possibleThirdNumber, $aInput)) {
				array_push($aOutput, "{$firstNumber} , {$secondNumber} , {$possibleThirdNumber}<br>");
			}
		}
	}
	echo implode("",array_unique($aOutput));
}
//find_combination_sum(array(2, 3, 1, -2, -1, 0, 2, -3, 0), 0);


/** FIND MAX SUM */
function find_max_sum($aInput) {
	$iCummSum = 0;
	$iMaxSum = 0;
	$iMaxStartIndex = 0;
	$iMaxLastIndex = 0;
	for ($i = 0; $i < sizeof($aInput); $i++) {
		$iCummSum += $aInput[$i];
		if ($iCummSum > $iMaxSum) {
			$iMaxSum = $iCummSum;
			$iMaxStartIndex = !empty($iNewMaxStartIndex) ? $iNewMaxStartIndex : $iMaxStartIndex;
			$iMaxLastIndex = $i;
		} else if ($iCummSum < 0) {
			$iCummSum = 0;
			$iNewMaxStartIndex = $i + 1;
		}
	}
	echo $iMaxSum . " : " . $iMaxStartIndex . " : " . $iMaxLastIndex;
}
//find_max_sum(array(-1, 3, -5, 4, 6, -1, 2, -7, 13, -3));

/** FIND LONGEST COMMON STRING IN ARRAY */
function find_longest_common_string_in_array($aInput) {
	$aInput = array_map('trim', array_map('strtolower', $aInput));
	$sort_function    = function ($a, $b) {if(strlen($a) == strlen($b)){ return strcmp($a, $b);} return (strlen($a) > strlen($b)) ? 1 : -1;};
	usort($aInput, $sort_function);
	$aLongestCommonString = array();
	$aShortestString = str_split(array_shift($aInput));
	while (sizeof($aShortestString)) {
		array_unshift($aLongestCommonString, '');
		foreach ($aShortestString as $iC => $char) {
			foreach ($aInput as $iW => $word) {
				if (!strstr($word, $aLongestCommonString[0] . $char)) {
					break 2;
				}
			}
			$aLongestCommonString[0] .= $char;
		}
		array_shift($aShortestString);
	}
	usort($aLongestCommonString, $sort_function);
	echo strtoupper(array_pop($aLongestCommonString));
}
//find_longest_common_string_in_array(array('PTT757LP4','PTT757LA','PCT757LB','PCT757LP4EV'));

/** FIND LONGEST COMMON STRING */
function find_longest_common_string($strA, $strB) {
	if(strlen($strA) <= strlen($strB)) {$sStr = strtolower($strA); $lStr = strtolower($strB);} else {$sStr = strtolower($strB); $lStr = strtolower($strA);}
	$sort_function    = function ($a, $b) {if(strlen($a) == strlen($b)){ return strcmp($a, $b);} return (strlen($a) > strlen($b)) ? 1 : -1;};
	$aLongestCommonString = array();
	$aStringS = str_split($sStr);
	while (sizeof($aStringS)) {
		array_unshift($aLongestCommonString, '');
		foreach ($aStringS as $iC => $char) {
			if (!strstr($lStr, $aLongestCommonString[0] . $char)) {
				break 1;
			}
			$aLongestCommonString[0] .= $char;
		}
		array_shift($aStringS);
	}
	usort($aLongestCommonString, $sort_function);
	echo strtoupper(array_pop($aLongestCommonString));
}
//find_longest_common_string('PTT757P4','PTT757LA');

/** CHECK BRACKETS */
function brackets($str) {
	$brackets = '()[]{}';
	$aBracketsBuffer = array();
	for ($i = 0; $i < strlen($str); $i++) {
		if (strstr($brackets, $str[$i])) { 
			if (strpos($brackets, $str[$i]) % 2 == 0) { // Open brackets
				$aBracketsBuffer[] = $str[$i]; 
			} else { // Close brackets
				$lastOpenBracket = array_pop($aBracketsBuffer); 
				if (strpos($brackets, $lastOpenBracket) + 1 != strpos($brackets, $str[$i])) {
					return false;
				}
			}
		}
	}
	if (!empty($aBracketsBuffer)) {
		return false;
	}
	return true;
}
//var_dump(brackets('{(a+b)[c+d]}')); // True - Correct mathematical open and close

$array = array('a', 'b', 'c', 'd', 'e', 'f');
function shiftit($array, $pos = 1, $dir = 'left') {
	$temp = array();
	$pos = $pos%sizeof($array);
	foreach ($array as $key => $arr) {
		$newpos = ($dir == 'left') ? $key - $pos : $key + $pos; 
		if ($dir == 'left') {
			$temp[($newpos >= 0) ? $newpos : sizeof($array) + $newpos] = $arr;
		} else {
			$temp[($newpos < sizeof($array)) ? $newpos : sizeof($array) - $newpos + 1] = $arr;
		}
	}
	ksort($temp);
	return $temp;
}
$array = shiftit($array, 2, 'left');
//print_r($array); // Array ( [0] => c [1] => d [2] => e [3] => f [4] => a [5] => b ) 

/** Shuffle */
function shuffle_array($array) {
	
	for($i = 0; $i < sizeof($array); $i++) {
		$rand = rand(0, sizeof($array)-1);
		$temp = $array[$rand];
		$array[$rand] = $array[$i];
		$array[$i] = $temp;	
	}
	
	return $array;
}

/** Anagram */
function anagram($str1, $str2) {
	if (strlen($str1) != strlen($str2)) {
		return false;
	}
	for($i = 0; $i < strlen($str1); $i++) { 
		if (!isset($aTemp[$str1[$i]])) {
			$aTemp[$str1[$i]] = 0;
		}
		$aTemp[$str1[$i]]++;
		if (!isset($aTemp[$str2[$i]])) {
			$aTemp[$str2[$i]] = 0;
		}
		$aTemp[$str2[$i]]--; 
	}
	foreach ($aTemp as $key => $count) {
		if (!empty($count)) {
			return false;
		}
	}
	return true;
}
//$isAnagram = anagram($str1, $str2); var_dump($isAnagram);

/** Array to Json : json_encode */
$array = array('name' => 'satish', 'edu' => array('school' => 'myschool', 'gpa' => 4), 'like' => 'php');
function array_to_json($array) {
    $str .= '{';
    $size = 0;
    foreach ($array as $k => $v) {
        $size++;
        if ($size > 1) {
            $str .= ",";
        }
        $str .= "'{$k}':";
        if (is_array($v)) {
           
            $str .= array_to_json($v);
        } else {
            $str .= "'{$v}'";
        }
    }
    $str .= '}'; 
    return $str;
}
//$str = array_to_json($array); echo $str;

/** REMOVE DUPLICATE FROM SORTED ARRAY */
$array = array(1,2,3,4,4,5,5,5,6,7,7,8,9);
$j = 0; $i = 1;
while ($i < sizeof($array)) {
	if ($array[$j] == $array[$i]) {
		$i++;
	} else {
		$array[++$j] = $array[$i++];
	}
}
//print_r(array_slice($array, 0, $j+1));

/** Array sort based on number digit position */
$num = array(32,2,45,65,9,66,113,212,101);
function sort_array_num(&$num) {
	$sort_function    = function ($a, $b) use ( &$sort_function ){
		if(substr($a, 0, 1) == substr($b, 0, 1)){ 
			return $sort_function(substr($a, 1, 1), substr($b, 1, 1));
		}
		return (substr($a, 0, 1) > substr($b, 0, 1)) ? 1 : -1;
	};
	usort($num, $sort_function);
	return $num;
}
//sort_array_num($num); print_r($num);

/** MERGE SORT ARRAYS */
function merge($left, $right)
{
	$result = array();
	while(count($left) > 0 || count($right) > 0)
	{
		if(count($left) > 0 && count($right) > 0)
		{
			if($left[0] <= $right[0]){
				$result[] = array_shift($left);
			}
			else{
				$result[] = array_shift($right);
			}
		}
		elseif(count($left) > 0){
			$result[] = array_shift($left);
		}
		else{
			$result[] = array_shift($right);
		}	
	}
	return $result;
}

/** ALL COMBINATION SUBSET NUMBER */
//all_subset(4);
function all_subset($num, $out = '') {
	if ($num == 0) {
		echo $out . "\n";
	} else {
		for($i=1; $i<=$num; $i++)
			all_subset($num-$i, $out . '' . $i);
	}
}

/** SEQUENCE TO SUM */
//$array = array(1,12,3,4,6,10,8,11); //28 => true
function sequence_sum($array, $T) {
	$j = 0;
	$sum = 0;
	for($i = 0; $i < sizeof($array); $i++) {
		while ($j < sizeof($array) && $sum < $T){
			$sum += $array[$j++];
		}
		if ($sum == $T) {
			return true;
		}
		$sum -= $array[$i];
	}
	return false;
}

/** Every second element is greater than neighbors */
//$array = array(1,2,3,4,6,7,8,11,9); // Eg. [1,4,5,2,3] => [1,4,2,5,3]
for($i = 1; $i < sizeof($array); $i+=2) { 
	if ($array[$i-1] > $array[$i]) {
		$temp = $array[$i-1];
		$array[$i-1] = $array[$i];
		$array[$i] = $temp;
	}
	if ($array[$i] < $array[$i+1] && $i < sizeof($array)-1) {
		$temp = $array[$i+1];
		$array[$i+1] = $array[$i];
		$array[$i] = $temp;
	}
}

/** NUM TO STR */
// Eg. 1:A, 2:B, 3:AA, 4:AB, 5:BA, 6:BB, 7:AAA .....
function int_to_str($num, $str = '') {
	if ($num == 0) {
		return '';
	}
	while ($num > 0) {
		if ($num%2 == 1) {
			$str .= 'A';
		} else {
			$str .= 'B';
			$num -= 1;
		}
		$num >>= 1;
	}
	echo $str;
}

/** CHAR FREQ IN STR */
$str = 'satishsays'; // saysahstis
function str_arrange_with_freq_appear_and_dist($str, $dist = 3) {
	$freq = array();
	for ($i = 0; $i < strlen($str); $i++) {
		$freq[$str[$i]]++;
	}
	arsort($freq);
	$i = 0; 
	while ($i < strlen($str)) {
		$count = $dist;
		foreach ($freq as $char => $f) {
			echo $char;
			$count--;
			$i++;
			$freq[$char]--;
			if ($freq[$char] == 0) {
				unset($freq[$char]);
			}
			if ($count == 0) {
				break;
			}
		} 
	}
}

/** TOWER OF HANOI or LUCAS TOWER */
function tower_of_hanoi($num, $source, $dest, $aux) {
	if($num == 1) {
		echo "{$source} => {$dest}\n";
		return;
	}
	tower_of_hanoi($num-1, $source, $aux, $dest);
	echo "{$source} => {$dest}\n";
	tower_of_hanoi($num-1, $aux, $dest, $source);
}
//tower_of_hanoi(3, 'S', 'D', 'A');

