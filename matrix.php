<?php
// Count house of 1 in matrix
/**
10010
00110
10000
11000
00001 
This is equal to 4 groups of 1
*/

$inputString = "10010
01110
01000
11000
00011";
echo $inputString . "\n";
$aInput = explode("\n", $inputString);

$count = 0;
for ($i = 0; $i < sizeof($aInput); $i++) {
	for ($j = 0; $j < strlen($aInput[$i]); $j++) {
		if ($aInput[$i][$j] == 1) {
			checkOnes($aInput, $i, $j, array()); 
			$count++;
		}
	}
}

echo "\n" . $count;
function checkOnes(&$aInput, $i, $j, $aBuffer) {
	
	if ($aInput[$i][$j] == 1) { 
		array_push($aBuffer, "{$i}-{$j}"); // Add to stack
	}
	$aInput[$i][$j] = 2;
	if ($aInput[$i+1][$j+1] == 1) {
		return checkOnes($aInput, $i+1, $j+1, $aBuffer);
	}
	if ($aInput[$i+1][$j] == 1) {
		return checkOnes($aInput, $i+1, $j, $aBuffer);
	}
	if ($aInput[$i+1][$j-1] == 1) {
		return checkOnes($aInput, $i+1, $j-1, $aBuffer);
	}
	if ($aInput[$i-1][$j+1] == 1) {
		return checkOnes($aInput, $i-1, $j+1, $aBuffer);
	}
	if ($aInput[$i-1][$j] == 1) {
		return checkOnes($aInput, $i-1, $j, $aBuffer);
	}
	if ($aInput[$i-1][$j-1] == 1) {
		return checkOnes($aInput, $i-1, $j-1, $aBuffer);
	}
	if ($aInput[$i][$j+1] == 1) {
		return checkOnes($aInput, $i, $j+1, $aBuffer);
	}
	if ($aInput[$i][$j-1] == 1) {
		return checkOnes($aInput, $i, $j-1, $aBuffer);
	}
	
	if (!empty($aBuffer)) {
		list($i, $j) = explode('-',array_pop($aBuffer));
		return checkOnes($aInput, $i, $j, $aBuffer);
	}
	
	return;
}
