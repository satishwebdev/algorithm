<?php

/** BUBBLE SORT */
for($i = sizeof($aArr)-1; $i >= 0; $i--) {
	for($j = 0; $j < $i; $j++) {
		if ($aArr[$j+1] < $aArr[$j]) {
			$temp = $aArr[$j];
			$aArr[$j] = $aArr[$j+1];
			$aArr[$j+1] = $temp;
		}
	}
}

/** SELECTION SORT */
for($i = 0; $i < sizeof($aArr) - 1; $i++) {
	$index = $i;
	for ($j = $i + 1; $j <= sizeof($aArr) - 1; $j++) {
		if ($aArr[$index] > $aArr[$j]) {
			$index = $j;
		}
	}
	if ($index != $i) {
		$temp = $aArr[$index];
		$aArr[$index] = $aArr[$i];
		$aArr[$i] = $temp;
	}
}

/** INSERTION SORT */
for($i = 1; $i <= sizeof($aArr) - 1; $i++) {
	for ($j = $i; $j > 0; $j--) {
		if ($aArr[$j] < $aArr[$j-1]) {
			$temp = $aArr[$j];
			$aArr[$j] = $aArr[$j-1];
			$aArr[$j-1] = $temp;
		}
	}
}

/** QUICK SORT */
function quick_sort($low, $high, &$array) {
	$i = $low;
	$j = $high;
	$pivot = $array[(int)( ($low+$high)/2 )];
	while($i <= $j) {
		while ($array[$i] < $pivot) { 
			$i++;
		} 
		while ($array[$j] > $pivot) { 
			$j--;
		} 
		if ($i <= $j) {
			$temp = $array[$i];
			$array[$i] = $array[$j];
			$array[$j] = $temp;
			$i++;
			$j--;
		}
	}
	if ($low < $j) {
		quick_sort($low, $j, $array);
	}
	if ($i < $high) {
		quick_sort($i, $high, $array);
	}
	return $array;
}

/** MERGE SORT */
function merge_sort($array)
{
	$length = count($array);
	if($length <= 1)
	{
		return $array;
	}
	$left = $right = $result = array();
	$middle = round($length / 2, 0);
	for($i=0; $i<$middle; $i++)
	{
		$left[] = $array[$i];
	}
	for($j=$middle; $j<$length; $j++)
	{
		$right[] = $array[$j];
	}
	$left = merge_sort($left);
	$right = merge_sort($right);
	$result = merge($left, $right);
	return $result;
}

function merge($left, $right)
{
	$result = array();
	while(count($left) > 0 || count($right) > 0)
	{
		if(count($left) > 0 && count($right) > 0)
		{
			if($left[0] <= $right[0]){
				$result[] = array_shift($left);
			}
			else{
				$result[] = array_shift($right);
			}
		}
		elseif(count($left) > 0){
			$result[] = array_shift($left);
		}
		else{
			$result[] = array_shift($right);
		}	
	}
	return $result;
}